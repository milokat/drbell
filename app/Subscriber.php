<?php

namespace App;
use Hashids;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $table = 'subscribers';
    protected $fillable = array('email');

    public function getTokenAttribute()
    {
        return Hashids::encode($this->id);
    }
}
