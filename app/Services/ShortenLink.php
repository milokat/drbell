<?php

namespace App\Services;

use Mremi\UrlShortener\Model\Link;
use Mremi\UrlShortener\Provider\Google\GoogleProvider;

class ShortenLink
{
  /**
   * Return the content of the Site Map
   */
  public function shortenUrl($url)
	{		
			$link = new Link;
			$link->setLongUrl($url);
			
			$googleProvider = new GoogleProvider( 
				env('SHORT_URL_API'), //LOCATED IN ENV FILE
      	array('connect_timeout' => 1, 'timeout' => 1)
			);
			
			$googleProvider->shorten($link);

			return $link;
	}

}