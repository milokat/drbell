<?php

namespace App\Services\Mailers;
use App\Subscriber;
use Illuminate\Http\Request;
use Hashids;

class SubscriberMailer extends Mailer
{

    protected $user;
    
    function __construct(Request $request)
    {
        $email = $request->get('email', false);
        $this->user = Subscriber::firstOrCreate(['email' => $email]);
    }
    
    public function thankyou()
    { 
        $encodedId = Hashids::encode($this->user->id);

        $subject    = 'Welcome to the site!';
        $view       = 'emails.thankyou';
        $data       = array( 'email' => $encodedId);

        $this->emailTo($this->user, $view, $data, $subject);
    }
	
    public function ping()
    {
        $subject    = 'hello';
        $view       = 'emails.ping';
        $data       = ['enter view data here'];

        $this->emailTo($this->user, $view, $data, $subject);
    }  

//    public function confirm()
//    {
//        $subject    = 'CON-Firrm!';
//        $view       = 'emails.confirm';
//        $data       = ['enter view data here'];
//        
//        $this->emailTo($this->user, $view, $data, $subject);
//    }
//    
//    public function contact()
//    {
//        $subject    = 'CON-Firrm!';
//        $view       = 'emails.contact';
//        $data       = ['enter view data here'];
//        
//        $this->emailTo($this->user, $view, $data, $subject);
//    }
//    
//    public function invite()
//    {
//        $subject    = 'CON-Firrm!';
//        $view       = 'emails.invite';
//        $data       = ['enter view data here'];
//        
//        $this->emailTo($this->user, $view, $data, $subject);
//    }
//    
//    public function invoice()
//    {
//        $subject    = 'CON-Firrm!';
//        $view       = 'emails.invoice';
//        $data       = ['enter view data here'];
//        
//        $this->emailTo($this->user, $view, $data, $subject);
//    }
//     
//    public function progress()
//    {
//        $subject    = 'CON-Firrm!';
//        $view       = 'emails.progress';
//        $data       = ['enter view data here'];
//        
//        $this->emailTo($this->user, $view, $data, $subject);
//    }
//    
//    public function reignite()
//    {
//        $subject    = 'CON-Firrm!';
//        $view       = 'emails.reignite';
//        $data       = ['enter view data here'];
//        
//        $this->emailTo($this->user, $view, $data, $subject);
//    }
//    
//    public function survery()
//    {
//        $subject    = 'CON-Firrm!';
//        $view       = 'emails.survery';
//        $data       = ['enter view data here'];
//        
//        $this->emailTo($this->user, $view, $data, $subject);
//    }
//    
//    public function upsell()
//    {
//        $subject    = 'CON-Firrm!';
//        $view       = 'emails.upsell';
//        $data       = ['enter view data here'];
//        
//        $this->emailTo($this->user, $view, $data, $subject);
//    }
//    
//    public function welcome()
//    {
//        $subject    = 'CON-Firrm!';
//        $view       = 'emails.welcome';
//        $data       = ['enter view data here'];
//        
//        $this->emailTo($this->user, $view, $data, $subject);
//    }

}