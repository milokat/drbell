<?php

namespace App\Services\Mailers;
use App\Subscriber;
class Newsletter extends Mailer
{
    
    protected $user;
    
    function __construct()
    {
        $this->user = Subscriber::get();
    }
    
    public function newsletter()
    { 
        $subject    = 'Our News Letter!';
        $view       = 'emails.newsletter.index';
        $data       = ['enter view data here'];

        foreach ($this->user as $user) 
            {
              $this->emailTo($user, $view, $data, $subject);
            }      
    }    

}