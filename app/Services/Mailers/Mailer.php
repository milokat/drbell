<?php
             
namespace App\Services\Mailers;

use Illuminate\Support\Facades\Mail;

abstract class Mailer                                               
      {     
            public function emailTo($person, $view, $data, $subject)
            {
//                dd($person);
//                dd($view); 
//                dd($data);
//                dd($subject);   
							
                     Mail::send($view, $data, function($message) use($person, $subject, $data)
                 {        
                       $message->to($person->email)
                               ->subject($subject);
//                     dd($data);
                 });
            }
        } 