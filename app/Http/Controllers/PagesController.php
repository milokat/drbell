<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Map;
use App\Http\Controllers\Controller;
use GoogleMaps;
use App\Services\ShortenLink;

class PagesController extends Controller
{
    public function showHome(Map $map, Request $request, ShortenLink $shortenLink)
    {
			
			$lat = $map->getLat();
			$lng = $map->getLng();
			$link = $shortenLink->shortenUrl($request->url());
			$shortLink = $link->getShortUrl();

      	return view('pages.layouts.index', compact('lat', 'lng', 'shortLink'));
    }

    public function showServices(Map $map, Request $request, ShortenLink $shortenLink)
    {
			
			$lat = $map->getLat();
			$lng = $map->getLng();
			$link = $shortenLink->shortenUrl($request->url());
			$shortLink = $link->getShortUrl();

      	return view('pages.services', compact('lat', 'lng', 'shortLink'));
    }
	
		public function showUnsubscribe(Map $map)
    {
			
			$lat = $map->getLat();
			$lng = $map->getLng();
			
      	return view('unsubscribe', compact('lat', 'lng'));
    }

			public function showUpload()
    {
			
      	return view('upload');
    }
}
