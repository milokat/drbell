<?php
namespace App\Http\Controllers;
use Activity;
use App\Jobs\BlogIndexData;
use App\Http\Requests;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use App\Services\SiteMap;
use App\Services\ShortenLink;
use Hashids;
use App\Services\RssFeed;
use Mremi\UrlShortener\Model\Link;


class BlogController extends Controller
{
  public function index(Request $request)
  { 
    $tag = $request->get('tag');
    $data = $this->dispatch(new BlogIndexData($tag));
		// Click BLOG
    $layout = $tag ? Tag::layout($tag) : 'pages.layouts.blog';
		//SCROLL BLOG - Remove Pager Styles
//		$layout = $tag ? Tag::layout($tag) : 'sandbox.scroll-blog';
    return view($layout, $data);
  }

  public function showPost($slug, Request $request, ShortenLink $shortenLink)
  {
    $post = Post::with('tags')->whereSlug($slug)->firstOrFail();
    $tag = $request->get('tag');
		
		$link = $shortenLink->shortenUrl($request->url());
		$shortLink = $link->getShortUrl();
		
    if ($tag) {
        $tag = Tag::whereTag($tag)->firstOrFail();
    }
		
    return view($post->layout, compact('post', 'tag', 'slug', 'shortLink'));
  }

	public function rss(RssFeed $feed)
	{
		$rss = $feed->getRSS();

		return response($rss)
			->header('Content-type', 'application/rss+xml');
	}

	public function siteMap(SiteMap $siteMap)
	{
		$map = $siteMap->getSiteMap();

		return response($map)
			->header('Content-type', 'text/xml');
	}
}