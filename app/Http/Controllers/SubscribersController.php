<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscriber;
use App\Http\Requests;
use App\Http\Requests\SubscriberCreateRequest;
use App\Http\Requests\SubscriberRemoveRequest;
use App\Http\Controllers\Controller;
use Activity;
use Hashids;

class SubscribersController extends Controller
{   
    protected $fields = [
    'email' => '',
    ];

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    $data = [];
			
    foreach ($this->fields as $field => $default) {
      $data[$field] = old($field, $default);
    }
    return view('admin.subscribers.create', $data);
		}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubscriberCreateRequest $request)
    {
        $subscriber = Subscriber::create($request->subscriberFillData());

        return redirect('/admin/subscribers')
            ->withSuccess('New Subscriber!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $decodedId = Hashids::decode($id); 
        $subscriber = Subscriber::findOrFail($decodedId[0]);
        $data = ['id' => $decodedId[0]];
        foreach (array_keys($this->fields) as $field) {
          $data[$field] = old($field, $subscriber->$field);
        }  
				return view('pages.subscribers.edit', $data);
			}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subscriber = Subscriber::findOrFail($id['id']);

        $subscriber->delete();
        Activity::log('User Unsubscribed');
			
        return redirect('/home')
            ->withSuccess('Subscriber Removed.');
    }
    
    public function destroyByEmail(SubscriberRemoveRequest $request)
    {
				$email = $request->only('email');
			
				Subscriber::where(['email' => $email['email']])->delete(); 
				Activity::log('User Unsubscribed');
			
				return back()
						->withSuccess("Sorry To See You Go! :(");
    }
}
