<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Jobs\SendThankYouEmail;
use App\Http\Requests;
use App\Subscriber;
use Activity;

use App\Services\Mailers\SubscriberMailer;

class SubscriberMailerController extends Controller
{
    protected $mail;
    function __construct(SubscriberMailer $mail)
    {
        $this->mail = $mail;
    }
    
    public function thankyou(Request $request)
    {
        $email = $request->only('email');

        $this->mail->thankyou($email);
        Activity::log('New Subscriber and Email Sent!');
			
        return back()
        ->withSuccess("Thank you for joining!");
    }
    
    public function ping(Request $request)
    {
        $email = $request->only('email');
        $this->mail->ping($email);
        return back()
        ->withSuccess("Thank you for joining!");
    }
    
    public function index()
    {   
        
    }
    
    public function delete()
    {
        //
    }
    
//    public function sendThankYouEmail()
//      { 
//
//        $this->dispatch(new SendThankYouEmail);
//        
//        return 'Sent!';
//
//      }
}

