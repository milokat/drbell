<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\ContactMeRequest;
use Illuminate\Support\Facades\Mail;
use App\Map;

class ContactController extends Controller
{
  /**
   * Show the form
   *
   * @return View
   */
  public function showForm(Map $map)
  {
		$lat = $map->getLat();
		$lng = $map->getLng();

    return view('pages.contact', compact('lat', 'lng'));
  }

  /**
   * Email the contact request
   *
   * @param ContactMeRequest $request
   * @return Redirect
   */
  public function sendContactInfo(ContactMeRequest $request)
  {
    $data = $request->only('name', 'email', 'phone');
    $data['messageLines'] = explode("\n", $request->get('message'));

    Mail::queue('emails.contact', $data, function ($message) use ($data) {
      $message->subject('Blog Contact Form: '.$data['name'])
              ->to(config('blog.contact_email'))
              ->replyTo($data['email']);
    });

    return back()
        ->withSuccess("Thank you for your message. It has been sent.");
  }

}