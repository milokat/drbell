<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Subscriber;
use App\Http\Requests;
use App\Http\Requests\SubscriberCreateRequest;
use App\Http\Requests\SubscriberRemoveRequest;
use App\Http\Controllers\Controller;

class SubscribersAdminController extends Controller
{   
    protected $fields = [
    'email' => '',
  ];
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscribers = Subscriber::all();
        return view('admin.subscribers.index')
            ->withSubscribers($subscribers);
    }
     /**
     * Delete Group of Ids.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyGroup(Request $request)
    {
    $ids = $request->input('subscriber'); 
//        dd($ids);
    Subscriber::destroy($ids);

    return redirect('admin/subscribers');
    }
}
