<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Logic\Image\ImageRepository;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
	protected $image;

	public function __construct(ImageRepository $imageRepository)
	{
			$this->image = $imageRepository;
	}

	public function getUpload()
	{
			return view('admin.upload.uploader');
	}

	public function postUpload()
	{
			$photo = Input::all();
			$response = $this->image->upload($photo);
			return $response;
	}

	public function deleteUpload()
    {
        $filename = Input::get('id');
        if(!$filename)
        {
            return 0;
        }

        $response = $this->image->delete( $filename );

        return $response;
    }
}
