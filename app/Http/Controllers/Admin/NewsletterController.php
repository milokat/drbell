<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Jobs\SendThankYouEmail;
use App\Http\Requests;
use App\Subscriber;

use App\Services\Mailers\Newsletter;

class NewsletterController extends Controller
{
    protected $mail;
    function __construct(Newsletter $mail)
    {
        $this->mail = $mail;
    }
    
    public function newsletter()
    {
        $this->mail->newsletter();
        return 'Sent!';
    }
    
    public function index()
    {   
        
    }
    
    public function delete()
    {
        //
    }
    
    public function sendThankYouEmail()
      { 

        $this->dispatch(new SendThankYouEmail);
        
        return 'Sent!';

      }
}

