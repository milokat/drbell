<?php
// Pages

get('blog', 'BlogController@index');
get('blog/{slug}', 'BlogController@showPost');
get('rss', 'BlogController@rss');
get('sitemap.xml', 'BlogController@siteMap');

$router->get('/', 'PagesController@showHome');
				 get('home', 'PagesController@showHome');
				 get('services', 'PagesController@showServices');
				 get('contact', 'ContactController@showForm');
         post('contact', 'ContactController@sendContactInfo');
				 get('unsubscribe', 'PagesController@showUnsubscribe');

// Mailers
post('admin/mail/ping', 'SubscriberMailerController@ping');
post('/mail/thankyou', 'SubscriberMailerController@thankyou');

// Subscribers
get('/{id}/unsubscribe', 'SubscribersController@edit');
delete('/subscribers/unsubscribe', 'SubscribersController@destroyByEmail');
resource('subscribers', 'SubscribersController', ['except' => ['show', 'edit']]); 

// Logging in and out
get('/auth/login', 'Auth\AuthController@getLogin');
post('/auth/login', 'Auth\AuthController@postLogin');
get('/auth/logout', 'Auth\AuthController@getLogout');

//// Registration routes
//Route::get('auth/register', 'Auth\AuthController@getRegister');
//Route::post('auth/register', 'Auth\AuthController@postRegister');

// Admin area
get('admin', function () {
  return redirect('/admin/post');
});

$router->group([
  'namespace' => 'Admin',
  'middleware' => 'admin:admin',
], function () {
			
			get('admin/uploader', 'ImageController@getUpload');
			post('admin/uploaderImg', ['as' => 'upload-post', 'uses' =>'ImageController@postUpload']);
			post('admin/uploader/delete', ['as' => 'upload-remove', 'uses' =>'ImageController@deleteUpload']);
	
      get('admin/subscribers', 'SubscribersAdminController@index');
      delete('admin/subscribers/unsubscribeGroup', 'SubscribersAdminController@destroyGroup');

      resource('admin/post', 'PostController', ['except' => 'show']);
      resource('admin/tag', 'TagController', ['except' => 'show']);
      resource('admin/activitylog', 'ActivityLogController', ['except' => 'show']);
      post('admin/newsletter/newsletter', 'NewsletterController@newsletter');
      get('admin/upload', 'UploadController@index');
      post('admin/upload/file', 'UploadController@uploadFile');
      delete('admin/upload/file', 'UploadController@deleteFile');
      post('admin/upload/folder', 'UploadController@createFolder');
      delete('admin/upload/folder', 'UploadController@deleteFolder');
});

get('images/{filename}', function ($filename)
{
    $path = storage_path() . '/' . $filename;

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

get('thankyou', function () {
  return view('/emails/thankyou');
});