<?php

namespace App\Http\Requests;
use Carbon\Carbon;
use App\Http\Requests\Request;

class SubscriberCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:subscribers,email',
        ];
    }
   /**
   * Return the fields and values to create a new post from
   */
  public function subscriberFillData()
  {
    return [
      'email' => $this->email,
    ];
  }
}
