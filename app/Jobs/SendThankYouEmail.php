<?php

namespace App\Jobs;
use App\Subscriber;
use App\Jobs\Job;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendThankYouEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
         {     
              //We first get the all data from our subscribers//database
               $subscribers = Subscriber::all(); 
//             dd($subscribers);
        
               foreach ($subscribers as $subscriber) {

                     //Now we send an email to each subscriber
                     Mail::queue('emails.thankyou', [$subscriber->email], function($message) use ($subscriber) 
                 {
                       $message->from('us@oursite.com', 'Our Name');
                       $message->to($subscriber->email, $name = null);
                 });

               }
                   $this->delete();
            return 'DONE';
        } 
    }
}

