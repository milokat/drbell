<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GoogleMaps;

class Map extends Model
{
   public function getLat()
	{
		 
		$response = GoogleMaps::load('geocoding')
        ->setParam (['address' => env('BUSINESS_ADDRESS')])
        ->get();
		
		$mapData = json_decode( $response, true );
		
		$mapResults = $mapData['results'];

		$lat = $mapResults[0]['geometry']['location']['lat'];
			
		return $lat;
		
	}

   public function getLng()
	{
		 
		$response = GoogleMaps::load('geocoding')
        ->setParam (['address' => env('BUSINESS_ADDRESS')])
        ->get();
		
		$mapData = json_decode( $response, true );
		
		$mapResults = $mapData['results'];

		$lng = $mapResults[0]['geometry']['location']['lng'];
		 
		return $lng;
		
	}

}