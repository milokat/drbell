<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
				protected $table = 'activity_logs';
        protected $fillable = [
            'user_id', 'text', 'ip_address', 'created_at', 'updated_at'
            ];
}
