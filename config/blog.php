<?php
return [
    'name' => "Roger Bell Chiropractic",
    'title' => "Dr. Bell's Blog.",
    'subtitle' => 'Catchy Subtitle Goes Here',
    'tagline' => "This Can Be Anything",
    'description' => 'This is my meta description',
    'author' => 'Milo',
	  'signature' => '- Dr. Bell',
    'page_image' => 'test2.jpg',
    'posts_per_page' => 5,
    'rss_size' => 25,
    'contact_email' => env('MAIL_FROM'),
    'uploads' => [
        'storage' => 'local',
        'webpath' => '/uploads/',
    ],
];