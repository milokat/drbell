<?php
return [
		'address' => env('BUSINESS_ADDRESS'),
		'phone' => env('PHONE_NUMBER'),
		'fax' => env('FAX'),
		'email' => env('EMAIL'),

		'facebook' => 'https://www.facebook.com/RogerBellChiropractic/?ref=hl',
		'googleplus' => 'https://plus.google.com/b/111495457073107574823/111495457073107574823/about',
		'twitter' => 'https://twitter.com/RogerBellChiro',
		'linkedin' => 'https://www.linkedin.com/in/roger-bell-25b59ab2',
];