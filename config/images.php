<?php

return [
    'full_size'   => public_path(env('FULL_SIZE')) ,
    'icon_size'   => public_path(env('ICON_SIZE')) ,
];