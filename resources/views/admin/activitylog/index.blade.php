@extends('admin.layout')

@section('content')
  <div class="container-fluid">
    <div class="row page-title-row">
      <div class="col-md-6">
        <h3>Logs <small>» Listing</small></h3>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">

        @include('admin.partials.errors')
        @include('admin.partials.success')

        <table id="posts-table" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>User_ID</th>
              <th>Log</th>
              <th>IP Address</th>
              <th data-sortable="false">Created At</th>
              <th data-sortable="false">Updated At</th>
              <th data-sortable="false">Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($activity_logs as $log)
              <tr>
                <td>{{ $log->user_id }}</td>
                <td>{{ $log->text }}</td>
                <td>{{ $log->ip_address }}</td>
                <td>{{ $log->created_at }}</td>
                <td>{{ $log->updated_at }}</td>
                </td>
                <td>
                 @can('can_edit')
                  <button type="button" class="btn btn-danger btn-md"
                          data-toggle="modal" data-target="#modal-delete">
                    <i class="fa fa-times-circle"></i>
                    Delete
                  </button>
                 @endcan
                </td>
              </tr>
              
                         {{-- Confirm Delete --}}
              <div class="modal fade" id="modal-delete" tabIndex="-1">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">
                        ×
                      </button>
                      <h4 class="modal-title">Please Confirm</h4>
                    </div>
                    <div class="modal-body">
                      <p class="lead">
                        <i class="fa fa-question-circle fa-lg"></i>  
                        Are you sure you want to delete this log?
                      </p>
                    </div>
                    <div class="modal-footer">
                <div class="modal-footer">
                  <form action="/admin/activitylog/{{ $log->id }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button class="btn btn-xs btn-danger"> Delete</button>
                  </form>
                </div>
                  </div>
                </div>
              </div>     
              
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  
  
@stop

@section('scripts')
  <script>
    $(function() {
      $("#posts-table").DataTable({
        order: [[0, "desc"]]
      });
    });
  </script>
@stop