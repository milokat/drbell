<ul class="nav navbar-nav">
  <li><a href="/">Home</a></li>
  @if (Auth::check())
    <li @if (Request::is('admin/post*')) class="active" @endif>
      <a href="/admin/post">Posts</a>
    </li>
    <li @if (Request::is('admin/tag*')) class="active" @endif>
      <a href="/admin/tag">Tags</a>
    </li>
    @can('can_edit')
    <li @if (Request::is('admin/upload*')) class="active" @endif>
      <a href="/admin/upload">Uploads</a>
    </li>
    @endcan
    <li @if (Request::is('admin/subscribers*')) class="active" @endif>
      <a href="/admin/subscribers">Mailing List</a>
    </li>
    @can('can_edit')
    <li @if (Request::is('admin/activitylog*')) class="active" @endif>
      <a href="/admin/activitylog">Activity Log</a>
    </li>
    @endcan
  @endif
</ul>

<ul class="nav navbar-nav navbar-right">
  @if (Auth::guest())
    <li><a href="/auth/login">Login</a></li>
  @else
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
         aria-expanded="false">{{ Auth::user()->name }}
        <span class="caret"></span>
      </a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="/auth/logout">Logout</a></li>
      </ul>
    </li>
  @endif
</ul>