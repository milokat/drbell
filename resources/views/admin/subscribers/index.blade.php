@extends('admin.layout')
        
@section('content')
  <div class="container-fluid">
    <div class="row page-title-row">
      <div class="col-md-6">
        <h3>Mailing List <small>» Listing</small></h3>
      </div>
    </div>
  </div>
    <div class="row">
      <div class="col-sm-12">

        @include('admin.partials.errors')
        @include('admin.partials.success')     
                    
                 <div class="container">   
                    <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                    <form action="/admin/subscribers/unsubscribeGroup" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                     @foreach ($subscribers as $subscriber)
                    <input tabindex="1" type="checkbox" name="subscriber[]" id="{{$subscriber->id}}" value="{{$subscriber->id}}"> {{ $subscriber->email }} </input><br />
                     @endforeach  
                     <hr>
                     @can('can_edit')
                    <button type="button" class="btn btn-danger btn-sm"
                          data-toggle="modal" data-target="#modal-delete">
                    <i class="fa fa-times-circle"></i>
                    Delete All Selected
                    </button>
                     @endcan
                      {{-- Confirm Delete --}}
                      <div class="modal fade" id="modal-delete" tabIndex="-1">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">
                                ×
                              </button>
                              <h4 class="modal-title">Please Confirm</h4>
                            </div>
                            <div class="modal-body">
                              <p class="lead">
                                <i class="fa fa-question-circle fa-lg"></i>  
                                Are you sure you want to delete this Subscriber?
                              </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-md btn-danger">Delete Selected</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    </div>
                    <div class="col-md-2">
                        <a href="/subscribers/create" class="btn btn-success btn-md">
                          <i class="fa fa-plus-circle"></i> New Subscriber
                        </a>
                    </div>
                    </div>
                 </div>
  
{{--  
<div class="container">
    <ul>
        <li>    
            <select  name="subscribers" id="subscribers" >
            <option>Select Subscriber</option>
            @foreach ($subscribers as $subscriber)
            <option value="{{ $subscriber->email }}">{{ $subscriber->email }}</option>
            @endforeach
            </select>
        </li>
    </ul>
</div>

          {{--      <button type="button" class="btn btn-danger btn-xs"
                          data-toggle="modal" data-target="#modal-delete">
                    <i class="fa fa-times-circle"></i>
                    Delete
                </button>
                </td>
              </tr> --}}
              
              
               {{-- Confirm Delete --}}
   {{--    <div class="modal fade" id="modal-delete" tabIndex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">
                    ×
                  </button>
                  <h4 class="modal-title">Please Confirm</h4>
                </div>
                <div class="modal-body">
                  <p class="lead">
                    <i class="fa fa-question-circle fa-lg"></i>  
                    Are you sure you want to delete this Subscriber?
                  </p>
                </div>
                <div class="modal-footer">
                  <form action="/subscribers/{{ $subscriber->id }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button class="btn btn-xs btn-danger"> Delete</button>
                  </form>
                </div>
              </div>
            </div>
          </div>      --}}
   {{--       
<div class="container">
<form action="/admin/newsletter/newsletter" method="post">
  <input type="hidden" name="_token" value="{!! csrf_token() !!}">
  <br>
  <div class="row">
    <div class="form-group col-xs-12">
      <button type="submit" class="btn btn-default">Send Newsletter to all Subscribers</button>
    </div>
  </div>
</form>
</div>

<div class="container">
<form action="/admin/mail/thankyou" method="post">
  <input type="hidden" name="_token" value="{!! csrf_token() !!}">
  <br>
  <div class="row">
    <div class="form-group col-xs-12">
      <button type="submit" class="btn btn-default">Send Email to all Subscribers</button>
    </div>
  </div>
</form>
</div>

<div class="container">
<form action="/admin/mail" method="post">
  <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row control-group">
    <div class="form-group col-xs-12">
      <label for="email">Email Address</label>
      <input type="email" class="form-control" id="email" name="email"
             value="{{ old('email') }}">
    </div>
    </div>
  <br>
  <div class="row">
    <div class="form-group col-xs-12">
      <button type="submit" class="btn btn-default">Ping</button>
    </div>
  </div>
</form>
</div>
--}}
@stop

@section('scripts')
  <script>
    $(function() {
      $("#posts-table").DataTable({
        order: [[0, "desc"]]
      });
    });
  </script>
@stop  