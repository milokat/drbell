{{-- Form Starts Here --}}

<div class="form-group">
  <label for="title" class="col-md-3 control-label">
    Your Email
  </label>
  <div class="col-md-8">
    <input type="email" class="form-control" name="email"
           id="email" value="{{ $email }}">
  </div>
</div>        
        
        
{{-- Form Ends Here --}}
