@extends('pages.layouts.master', ['meta_description' => 'Contact Form'])

@section('page-header')
 
{{-- Page Header --}}
<header class="intro-header contactHeader"  style="background-image: url('/uploads/img/files-2.jpg')"  >
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
				<div class="site-heading">
					<h1>Get in Touch</h1>
					<hr class="small">
				</div>
			</div>
		</div>
	</div>
</header>

@stop

@section('content')

{{-- Contact Page Content --}}
<div id="contactPageContent">

	{{-- Contact Form Row --}}
	<div class="row">
		<div class="container contact">
		
			{{-- Intro Block --}}
			<div class="col-sm-6">
				@include('pages.partials.errors')
				@include('pages.partials.success')
							
				<h1>Questions?</h1>
				<p>
					Please, fill out the form to send us a message.
				</p>
				<p>You can request an appointment by calling our office during work hours, or feel free to send a message and we will return your call promptly by our next business day. </p>
				<table>
					<caption>Closed for Lunch - 12 p.m. to 1:30 p.m.</caption>
						<tbody>
							<tr>
								<th colspan="3">Mon. | Tues. | Thur.</th>
								<td>7am - 7pm</td>
							</tr>
							<tr>
								<th colspan="3">Wednesday</th>
								<td>Closed</td>
							</tr>
							<tr>
								<th colspan="3">Friday</th>
								<td>7am - 3pm</td>
							</tr>
						</tbody>
					</table>
			 </div>
			
			{{-- Contact Form --}}
			<form class="col-sm-6" action="/contact" method="post">
				<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<div class="row control-group">
						<div class="form-group col-xs-12">
							<label for="name">Name</label>
								<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
						</div>
					</div>
					<div class="row control-group">
						<div class="form-group col-xs-12">
							<label for="email">Email Address</label>
								<input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
						</div>
					</div>
					<div class="row control-group">
						<div class="form-group col-xs-12 controls">
							<label for="phone">Phone Number</label>
								<input type="tel" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
						</div>
					</div>
					<div class="row control-group">
						<div class="form-group col-xs-12 controls">
							<label for="message">Message</label>
								<textarea rows="5" class="form-control" id="message" name="message">
									{{ old('message') }}
								</textarea>
						</div>
					</div>
<br />
					<div class="form-group col-sm-2 col-sm-offset-1 col-xs-12">
						<button type="submit" class="btn btn-default">
							Send
						</button>
					</div>
			</form>
		</div>
	</div>
	
	{{-- About Us Row --}}
	<div class="row about"> 
		<div class="container-fluid">
			 <h2 class="underline">Meet Dr. Bell</h2>
					
					<div class="container-fluid pull-right">
						<img class="img-circle img-responsive img-center" src="/uploads/img/portrait-1.jpg" alt="">
					</div>
		     
					
<div class="container-fluid">
							<p><strong>Doctor Roger Bell</strong> received his Bachelor of Arts degree from Transylvania University in Lexington, Kentucky in 1974.</p> 
								
								<p>He commenced his post-graduate studies, receiving a degree in nuclear medicine from Ohio State University Hospital. After working in the field of nuclear scanning for several years, he decided to attend Chiropractic College. </p>
								
								<p>In 1980, Dr. Bell graduated with honors from Life Chiropractic College in Marietta, Georgia. He has been practicing since 1980, owning a family practice in New Port Richey since 1986, and a member of the Florida Chiropractic Association (FCA) since 1981.</p>
						</div>
								
		</div>
	</div>
	
</div>

@stop

@section('footer')

	@include('pages.partials.page-footer')
	
@stop

@section('scripts')

	@include('pages.partials.map.scripts')
	@include('pages.partials.social.scripts')
	
@stop