@extends('pages.layouts.master', ['meta_description' => 'Services Page'])

@section('content')
<div id="servicesPageContent">
{{-- Jumbotron Header --}}

	<header class="jumbotron">
	<div class="grad">
		<h1>Our Services!</h1>
			<p>Our services range from simple adjustments to improve joint and muscle movement to spinal manipulation and massages from our in-house, licensed massage therapists.</p>
	</div>
	</header>
{{-- The Shortlink --}}
<div class="row">
	<div class="container">
		<h5 style="line-height:1.4em;color:rgba(131, 171, 149, 1.0);" class="underline">
				Share our page - 
			<a style="color:rgba(  255, 140, 0, 0.8 ); font-size:16;" href="{{ $shortLink }}">
				{{ $shortLink }}
			</a>
		</h5>

					{{-- Subscriber Button --}}
			<div class="connectModal pull-right">
				<a href="#subscriberModal" class="btn btn-info btn-xs" data-toggle="modal" style="font-size:12px;">
				<i class="fa fa-plus"></i> 
					Join our Newsletter!
				</a>
			</div>
				@include('pages.partials.modals.subscribe')
	</div>
</div>

{{-- Page Features --}}
<div class="row text-center featurettes">
	<div class="container-fluid">
		<div class="col-lg-2 col-md-4 col-sm-6 hero-feature">
			<div class="thumbnail">
				<img src="http://placehold.it/800x500" alt="">
					<div class="caption">
						<h3>Hands-On<br />Adjustments</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<p>
							<a href="#" class="btn btn-default">More Info</a>
							</p>
					</div>
			</div>
		</div>

		<div class="col-lg-2 col-md-4 col-sm-6 hero-feature">
			<div class="thumbnail">
				<img src="http://placehold.it/800x500" alt="">
					<div class="caption">
						<h3>Spine<br />Decompression</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<p>
							<a href="#" class="btn btn-default">More Info</a>
							</p>
					</div>
			</div>
		</div>

		<div class="col-lg-2 col-md-4 col-sm-6 hero-feature">
			<div class="thumbnail">
				<img src="http://placehold.it/800x500" alt="">
					<div class="caption">
						<h3>Heat<br />Therapy</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<p>
							<a href="#" class="btn btn-default">More Info</a>
							</p>
					</div>
			</div>
		</div>

		<div class="col-lg-2 col-md-4 col-sm-6 hero-feature">
			<div class="thumbnail">
				<img src="http://placehold.it/800x500" alt="">
					<div class="caption">
						<h3>Therapeutic<br />Massage</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<p>
							<a href="#" class="btn btn-default">More Info</a>
							</p>
					</div>
			</div>
		</div>

		<div class="col-lg-2 col-md-4 col-sm-6 hero-feature">
			<div class="thumbnail">
				<img src="http://placehold.it/800x500" alt="">
					<div class="caption">
						<h3>Hands-on<br />Realignment</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<p>
							<a href="#" class="btn btn-default">More Info</a>
							</p>
					</div>
			</div>
		</div>

		<div class="col-lg-2 col-md-4 col-sm-6 hero-feature">
				<div class="thumbnail">
					<img src="http://placehold.it/800x500" alt="">
						<div class="caption">
							<h3>Ultrasound<br />Therapy</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
								<p>
								<a href="#" class="btn btn-default">More Info</a>
								</p>
						</div>
				</div>
			</div>
	</div>		
</div>

{{-- Insurance Information --}}
<div id="insurance" style="background-color:rgba( 70, 70, 70, 1); box-shadow: 0 0 5px rgba( 41, 59, 79, 0.8);">
	<div class="row">
		<h2 style="padding-left:50px; font-size:50px; color:white;">
			Insurance Information
		</h2>
	</div>
	<div class="container-fluid">
		<div class="col-sm-5">
			<p style="padding:15px; color:white;">We accept a variety of insurance plans found here, and you can speak with our staff if you need help paying out of pocket.
			</p>
		</div>
		<div class="col-sm-7">
			<h3 style="font-size:35px; color:white;">
			Common Accepted Providers
			</h3>
				<ul class="container-fluid" style="padding:25px; padding-left:50px; font-size:20px;">
					<li class="col-md-6"><a href="https://www.medicare.gov/" target="_blank">Medicare</a></li>
					<li class="col-md-6"><a href="http://www.bcbs.com/?referrer=https://www.google.com/?referrer=http://www.bcbs.com/" target="_blank">Blue Cross Blue Shield PPO</a></li>
					<li class="col-md-6"><a href="http://www.uhc.com/" target="_blank">United Healthcare</a></li>
					<li class="col-md-6"><a href="https://www.youroptimumhealthcare.com/" target="_blank">Optimum Healthcare</a></li>
					<li class="col-md-6"><a href="https://www.wellcare.com/en/Florida" target="_blank">WellCare Health Plans</a></li>
					<li class="col-md-6"><a href="https://www.freedomhealth.com/" target="_blank">Freedom Health</a></li>
					<li class="col-md-6"><a href="/" onclick="return false;">Automobile accidents</a></li>
					<li class="col-md-6"><a href="http://www.myfloridacfo.com/division/wc/" target="_blank">Workers' Compensation Insurance</a></li>
				</ul>
					<h4 style="color:rgba( 237, 230, 206, 1.0);">
						* if your insurance is not on this list, please call for more information.
					</h4>
		</div>
	</div>
</div>
</div>
@stop

@section('footer')

	@include('pages.partials.page-footer')
	
@stop

@section('scripts')
	
	@include('pages.partials.map.scripts')
	@include('pages.partials.social.scripts')
	
@stop