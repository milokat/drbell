{{-- Facebook --}}
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '915892435114852',
      xfbml      : true,
      version    : 'v2.5'
    });
  };
  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=915892435114852";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

{{-- LinkedIn --}}
<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>

{{-- Google --}}
<script src="https://apis.google.com/js/platform.js" async defer></script>

{{-- Twitter --}}
{{-- share --}}
<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)              [0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
</script>