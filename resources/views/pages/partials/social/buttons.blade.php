<div class="row">
	<div class="container">
	
		{{-- Facebook --}} 
	<div id="fb-root"></div>
	<div class="fb-share-button" data-href="http://www.danielreinecke.com/" data-layout="button"></div>
	
		{{-- Linkedin --}}
	<div>
		<script type="IN/Share" data-counter="right">
			LINKEDIN
		</script>
	</div>
	
		{{-- Google Plus --}}
	<div id="sharePost" class="demo g-interactivepost"
			data-clientid="841077041629.apps.googleusercontent.com"
			data-contenturl="https://developers.google.com/+/web/share/interactive"
			data-calltoactionlabel="INVITE"
			data-calltoactionurl="https://developers.google.com/+/web/share/interactive?invite=true"
			data-cookiepolicy="single_host_origin"
			data-prefilltext="Come learn about interactive posts with me!">
		<span class="icon">&nbsp;</span>
		<span class="label">Share!</span>
	</div>
		{{-- Twitter --}}
	<div>
		<a href="https://twitter.com/share" class="twitter-share-button"{count}>
			Tweet
		</a>
	</div>
	</div>
</div>