<div class="row">
	<div class="container-fluid shareLinks">
		{{-- Facebook --}} 
		<div id="sharePost" onclick="window.open( 'https://www.facebook.com/dialog/share?app_id=915892435114852&display=popup&href=http://danielreinecke.com/&redirect_uri=http://danielreinecke.com/', '', 'width=400, height=400')">
			<span class="icon facebook">&nbsp;</span>
			<span class="label">FACEBOOK</span>
		</div>
	
		{{-- Linkedin --}}
		<div id="sharePost" onclick="window.open('https://www.linkedin.com/cws/share?url={{ $shortLink }}', '', 'width=400, height=400')">
			<span class="icon linkedin">&nbsp;</span>
			<span class="label">LINKEDIN</span>
		</div>
	
		{{-- Google Plus --}}
		<div id="sharePost" class="demo g-interactivepost"
				data-clientid="841077041629.apps.googleusercontent.com"
				data-contenturl="https://developers.google.com/+/web/share/interactive"
				data-calltoactionlabel="INVITE"
				data-calltoactionurl="https://developers.google.com/+/web/share/interactive?invite=true"
				data-cookiepolicy="single_host_origin"
				data-prefilltext="Come learn about interactive posts with me!">
			<span class="icon google">&nbsp;</span>
			<a class="label">GOOGLE</a>
		</div>
		
		{{-- Twitter --}}
		<div id="sharePost"  onclick="window.open('https://twitter.com/intent/tweet?text={{ $shortLink }}', '', 'width=400, height=400')">
			<span class="icon twitter">&nbsp;</span>
			<span class="label">TWITTER</span>
		</div>

	</div>
</div>