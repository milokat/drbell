{{-- Navigation --}}
<nav class="navbar navBar navbar-fixed-top" role="navigation">
	<div class="container">
		{{-- Brand and toggle get grouped for better mobile display --}}
		<div class="navBar-head navbar-header page-scroll">
			<button type="button" style="margin-left:40px;" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-main">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
				<a class="navbar-brand logo" href="/">{{ config('blog.name') }}</a>
		</div>
		{{-- Collect the nav links, forms, and other content for toggling --}}
		<div class="navBar-menu collapse navbar-collapse" id="navbar-main">
			<ul class="nav navbar-nav">
				<li>
					<a class="navLink" href="/">Home</a>
				</li>
				<li>
					<a class="navLink" href="/services">Services</a>
				</li>
				<li>
					<a class="navLink" href="/blog">Blog</a>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a class="navLink" href="/contact">Contact</a>
				</li>
				 @can('admin_view')
				<li>
					<a class="adminLink" href="/admin">Admin</a>
				</li>
				 @endcan  
			</ul>
			<ul class="nav navbar-nav navbar-right">
				{{-- Social Connect Button --}} 
				<div id="connectMenu">
					<div class="row">
						<div class="connectPanelFixed">
							<div id="connectDropdown" class="dropdown" style="width:50%;">
								@include('pages.partials.social.connect-panel')
							</div>
						</div>
					</div>
				</div>
			</ul>
		</div>
	</div>
</nav>