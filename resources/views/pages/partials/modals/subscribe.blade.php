<div class="container-fluid">
	<div class="modal fade" id="subscriberModal" data-target="#subscriberModal" tabIndex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>    
					</button>
						<h4 class="modal-title"><strong></strong>Sign Up for Our Newsletter!</h4>    
				</div>
				<div class="container-fluid">
					<form action="/mail/thankyou" method="post">  {{-- Controller Route --}}
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">
							<div class="row control-group">
								<div class="form-group col-xs-12">
									<label for="email">Email Address</label>
										<input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}"></input>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-xs-12">
									<button type="submit" class="btn btn-default">Join Now</button>
								</div>
							</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
