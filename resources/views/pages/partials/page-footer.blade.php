{{-- GOOGLE MAPS --}}
<div class="row">
	<div class="container-fluid">
		@include('pages.partials.map.gmaps')
	</div> 
</div>

{{-- Contact Info --}}
<div class="row"> 
	<div class="container-fluid"> 
		<div class="row" style="background-color:rgba( 26, 38, 51, 1.0)">
			<div class="container">
				<ul class="list-group" style="padding:25px; text-align:center;">
					<li class="list-group-item"style="padding-left:10px;">
						<h3>{{ config('business-details.address') }}</h3>
					</li>
					<li class="list-group-item" style="padding-left:20px;">
								Phone: {{ config('business-details.phone') }}
					</li>
					<li class="list-group-item" style="padding-left:20px;">
								Fax: {{ config('business-details.fax') }}
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

{{-- Social Icons --}}
<div class="row" style="padding-top:80px;"> 
	<div class="container-fluid"> 
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
				<ul class="list-inline text-center">
					<li>
						<a href="{{ config('business-details.twitter') }}" data-toggle="tooltip"
							 title="My Twitter Page">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
							</span>
						</a>
					</li>
					<li>
						<a href="{{ config('business-details.facebook') }}" data-toggle="tooltip"
							 title="My Facebook Page">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
							</span>
						</a>
					</li>
					<li>
						<a href="{{ config('business-details.googleplus') }}" data-toggle="tooltip"
							 title="My Google+ Page">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
							</span>
						</a>
					</li>
					<li>
						<a href="{{ config('business-details.linkedin') }}" data-toggle="tooltip"
							 title="My LinkedIn Page">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
							</span>
						</a>
					</li>
				</ul>
			<br />
				<div style="text-align: center; font-size: 12px;">
					<p id="copyright">Copyright © {{ config('blog.author') }}</p>
				</div>
				<div style="text-align: center; font-size: 12px;">

						@if (Auth::guest())
						 <a href="/auth/login">Login</a>
						@else
						 <a href="/auth/logout">Logout</a>
						@endif

				</div>
			</div>
		</div>
	</div>
</div>
