{{-- GOOGLE MAP --}}
<div class="row" style="padding:50px;">
		<h3 style="font-size:40px;">Stop by for a visit!</h3>
</div>

{{-- Map --}}
<div class="col-sm-7" style="padding-bottom:50px;">
	<div id="googleMap" style="height:350px;"></div> {{-- Map Placement --}}
</div>

{{-- Form --}}	
<div class="col-sm-5">
	<form id="calculate-route" name="calculate-route" action="#" method="get">
			<label>Directions:</label>
		<br />
			<label class="hidden" for="from">From:</label>
				<input type="text" class="form-control" id="from" name="from" required="required" placeholder="Where are you coming from?" value="{{ old('from') }}" style="margin-bottom:4px" />
					<a id="from-link" href="#" class="formText pull-right" style="font-size:15px">
					Get my Location Automatically
					</a>
		<br />
			<label class="hidden" for="to">To:</label>
				<input class="form-control" type="text" id="to" name="to" required="required" placeholder="Ending Address Here" value="{{ config('business-details.address') }}" size="35" />
			
			{{-- Buttons --}}
			<div class="row" style="padding:20px">
				<button class="btn btn-default" type="submit" >Submit</button>
				<button class="btn btn-default" type="reset" >Reset</button>
				<div id="outputDistance" class="pull-right" style="padding-right:20px;"></div>
			</div>
			
			{{-- Output Time --}}
			<div class="row" style="font-size:25px; padding:25px;">		
				<div id="outputTime"></div>

			</div>
		</form>

		<div id="status"></div>
		<div id="output"></div>
		<p id="error"></p>

</div>