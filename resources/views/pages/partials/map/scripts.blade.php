<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js" ></script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>

<script>

// GOOGLE MAP
	$(document).ready(function() {
	function initialize() {
		
		var myLatlng = new google.maps.LatLng( {{ $lat }} , {{ $lng }} );
		var mapProp = {
			center:myLatlng,
			scrollwheel: false,
			zoom:14,
			mapTypeId:google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("googleMap"), mapProp); //Map Placement	
		
		var toInput = /** @type {!HTMLInputElement} */(
      document.getElementById('to'));
		var fromInput = /** @type {!HTMLInputElement} */(
      document.getElementById('from'));
			
			
		var autocomplete = new google.maps.places.Autocomplete(toInput);	
		var autocomplete = new google.maps.places.Autocomplete(fromInput);
		autocomplete.bindTo('bounds', map);
		var infowindow = new google.maps.InfoWindow();	
		
		var marker = new google.maps.Marker({
				position: myLatlng,
				map: map,
				title: 'Roger Bell Chiropractic'
						});
			
		autocomplete.addListener('place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      window.alert("Autocomplete's returned place contains no geometry");
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  // Why 17? Because it looks good.
    }
    marker.setIcon(/** @type {google.maps.Icon} */({
      url: place.icon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    }));
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    infowindow.open(map, marker);
  });

  // Sets a listener on a radio button to change the filter type on Places
  // Autocomplete.
  function setupClickListener(id, types) {
    var radioButton = document.getElementById(id);
    radioButton.addEventListener('click', function() {
      autocomplete.setTypes(types);
    });
  }
}
// INITIALIZE - MAP
	google.maps.event.addDomListener(window, 'load', initialize);
});
	
// GOOGLE MAP - CALCULATE ROUTE	
	function calculateRoute(from, to) {
	
		var myOptions = {
							zoom: 10,
							center: new google.maps.LatLng( {{ $lat }} , {{ $lng }} ),
							mapTypeId: google.maps.MapTypeId.ROADMAP
						};
		// Draw the map
		var mapObject = new google.maps.Map(document.getElementById("googleMap"), myOptions);
		var directionsService = new google.maps.DirectionsService();
		var directionsRequest = {
			origin: from,
      destination: to,
			optimizeWaypoints: true,
			provideRouteAlternatives: false,
			travelMode: google.maps.TravelMode.DRIVING,
			drivingOptions: {
				departureTime: new Date(),
				trafficModel: google.maps.TrafficModel.OPTIMISTIC
			}
		};
	 
		var starttime = new Date(Date.now() + 1000);

		var geocoder  = new google.maps.Geocoder();
		var startState;
		var currentState;
		var routeData;
		var index = 0;
		var stateChangeSteps = [];
		var borderLatLngs = {};
		var startLatLng;
		var endLatLng;

		directionsService = new google.maps.DirectionsService();
		directionsService.route(directionsRequest, init);
	 
		function init(data){
				routeData = data;
				displayRoute();
//				startLatLng = data.routes[0].start_location; // REMOVED - .legs[0]
//				endLatLng = data.routes[0].end_location; // REMOVED - .legs[0]
//				geocoder.geocode({location:data.routes[0].legs[0].start_location}, assignInitialState) 
		}

		function assignInitialState(data){
				startState = getState(data);
		}

		function getState(data){
				for (var i = 0; i < data.length; i++) {
						if (data[i].types[0] === "administrative_area_level_1") {
								var state = data[i].address_components[0].short_name;
						}
				}
				return state;
		}

		function compileStates(data, this_index){
				if(typeof(this_index) == "undefined"){
						index = 1;
						geocoder.geocode({location:data.routes[0].legs[0].steps[0].start_location}, compileStatesReceiver);
				}else{
						if(index >= data.routes[0].legs[0].steps.length){
								console.log(stateChangeSteps);
								index = 0;
								startBinarySearch();
								return;
						}
						setTimeout(function(){ 
										geocoder.geocode({location:data.routes[0].legs[0].steps[index].start_location}, compileStatesReceiver);
										$("#status").html("Indexing Step "+index+"...  ("+data.routes[0].legs[0].steps.length+" Steps Total)");
								}, 3000)
				}

		}

		function compileStatesReceiver(response){
					state = getState(response);
					console.log(state);
					if(state != currentState){
								currentState = state;
								stateChangeSteps.push(index-1);
					}
					index++; 
					compileStates(routeData, index);
				}

	 	var bounds = new google.maps.LatLngBounds;
		var markersArray = [];

		var origin = document.getElementById("from").value;
	 	var destination = document.getElementById("to").value;
	 	
		var destinationIcon = 'https://chart.googleapis.com/chart?' +
				'chst=d_map_pin_letter&chld=D|FF0000|000000';
		var originIcon = 'https://chart.googleapis.com/chart?' +
				'chst=d_map_pin_letter&chld=O|FFFF00|000000';

		var service = new google.maps.DistanceMatrixService;	 
	 
		service.getDistanceMatrix({
			origins: [origin],
    	destinations: [destination],
			travelMode: google.maps.TravelMode.DRIVING,
			unitSystem: google.maps.UnitSystem.IMPERIAL,
			avoidHighways: false,
			avoidTolls: false
		}, function(response, status) {
			 if (status !== google.maps.DistanceMatrixStatus.OK || response.rows[0].elements[0].status == "ZERO_RESULTS") 			{
				alert('Unable to find the distance via road.');
			} else {
				var originList = response.originAddresses;
				var destinationList = response.destinationAddresses;
				var outputTime = document.getElementById('outputTime');
				var outputDistance = document.getElementById('outputDistance');
				outputTime.innerHTML = '';
				outputDistance.innerHTML = '';
				
				deleteMarkers(markersArray);

				var showGeocodedAddressOnMap = function(asDestination) {
					var icon = asDestination ? destinationIcon : originIcon;
					return function(results, status) {
						if (status === google.maps.GeocoderStatus.OK) {
							map.fitBounds(bounds.extend(results[0].geometry.location));
							markersArray.push(new google.maps.Marker({
								map: map,
								position: results[0].geometry.location,
								icon: icon
							}));
						} else {
							alert('Geocode was not successful due to: ' + status);
						}
					};
				};

				for (var i = 0; i < originList.length; i++) {
					var results = response.rows[i].elements;
						geocoder.geocode({'address': originList[i]},
								showGeocodedAddressOnMap(false));
					
					for (var j = 0; j < results.length; j++) {
						geocoder.geocode({'address': destinationList[j]},
								showGeocodedAddressOnMap(true));
						
						outputTime.innerHTML += 'You are ' + 
							results[j].duration.text + ' away from us.' + '<br />';
						
						outputDistance.innerHTML += 'Distance: ' + 
							results[j].distance.text + '. ';
						
					}
				}
			}
		});
	

	 	function deleteMarkers(markersArray) {
			for (var i = 0; i < markersArray.length; i++) {
				markersArray[i].setMap(null);
			}
			markersArray = [];
		}
	 
		var stepIndex = 0;
		var stepStates = [];
		var binaryCurrentState = "";
		var stepNextState;
		var stepEndState;
		var step;

		var myLatLng = new google.maps.LatLng( {{ $lat }} , {{ $lng }} );
		var map = new google.maps.Map(document.getElementById('googleMap'), {
				zoom: 4,
				center: myLatLng
			});

		function displayRoute() {
			directionsDisplay = new google.maps.DirectionsRenderer();
			directionsDisplay.setMap(map);
			directionsDisplay.setDirections(routeData);
		}

		var orderedLatLngs = [];
		function startBinarySearch(iterating){
				if(stepIndex >= stateChangeSteps.length){
						for(step in borderLatLngs){
								for(state in borderLatLngs[step]){
										for(statename in borderLatLngs[step][state]){
											 $("#results").append("<br>Cross into "+statename+" at "+JSON.stringify(borderLatLngs[step][state][statename], null, 4));
											 orderedLatLngs.push([borderLatLngs[step][state][statename], statename]); 
										}
								}
						}
						compileMiles(true);
						return;

				}
				step = routeData.routes[0].legs[0].steps[stateChangeSteps[stepIndex]];
				console.log("Looking at step "+stateChangeSteps[stepIndex]);
				borderLatLngs[stepIndex] = {};
				if(!iterating){
						binaryCurrentState = startState;
				}
				geocoder.geocode({location:step.end_location}, 
						function(data){
								if(data === null){
										setTimeout(function(){startBinarySearch(true);}, 6000);
								}else{
										stepNextState = getState(data);
										stepEndState = stepNextState;
										binaryStage2(true);
								}
						});
		}

		var minIndex;
		var maxIndex;
		var currentIndex;
		function binaryStage2(init){
				if (typeof(init) != "undefined"){   
						minIndex = 0;
						maxIndex  = step.path.length - 1;    
				}
				if((maxIndex-minIndex)<2){
						borderLatLngs[stepIndex][maxIndex]={};
						borderLatLngs[stepIndex][maxIndex][stepNextState]=step.path[maxIndex];
						var marker = new google.maps.Marker({
								position: borderLatLngs[stepIndex][maxIndex][stepNextState],
								map: map,
						});
						if(stepNextState != stepEndState){
								minIndex = maxIndex;
								maxIndex = step.path.length - 1;
								binaryCurrentState = stepNextState;
								stepNextState = stepEndState;

						}else{
								stepIndex++;
								binaryCurrentState = stepNextState;
								startBinarySearch(true);
								return;
						}
				}
				console.log("Index starts: "+minIndex+" "+maxIndex);
				console.log("current state is "+binaryCurrentState);
				console.log("next state is "+ stepNextState);
				console.log("end state is "+ stepEndState);

				currentIndex = Math.floor((minIndex+maxIndex)/2);
				setTimeout(function(){
										geocoder.geocode({location:step.path[currentIndex]}, binaryStage2Reciever);
										$("#status").html("Searching for division between "+binaryCurrentState+" and "+stepNextState+" between indexes "+minIndex+" and "+maxIndex+"...") 
								}, 3000);


		}

		function binaryStage2Reciever(response){
				if(response === null){
						setTimeout(binaryStage2, 6000);
				}else{
						state = getState(response)
						if(state == binaryCurrentState){
								minIndex = currentIndex +1; 
						}else{
								maxIndex = currentIndex - 1
								if(state != stepNextState){
										stepNextState = state;
								}
						}
						binaryStage2();
				}
		}

		var currentStartPoint;
		var compileMilesIndex = 0;
		var stateMiles = {};
		var trueState;
		function compileMiles(init){
						if(typeof(init)!= "undefined"){
								currentStartPoint = startLatLng;
								trueState = startState;    
						}
						if(compileMilesIndex == orderedLatLngs.length){
								directionsRequest.destination = endLatLng;
						}else{
								directionsRequest.destination = orderedLatLngs[compileMilesIndex][0];
						}
						directionsRequest.origin = currentStartPoint;
						currentStartPoint = directionsRequest.destination;
						directionsService.route(directionsRequest, compileMilesReciever)

		}

		function compileMilesReciever(data){
				if(data===null){
						setTimeout(compileMiles, 6000);
				}else{
						if(compileMilesIndex == orderedLatLngs.length){
								stateMiles[stepEndState]=data.routes[0].legs[0].distance["text"];
								$("#results").append("<br><br><b>Distances Traveled</b>");
								for(state in stateMiles){
										$("#results").append("<br>"+state+": "+stateMiles[state]);
								}
								var endtime = new Date();
								totaltime = endtime - starttime;
								$("#results").append("<br><br>Operation took "+Math.floor(totaltime/60000)+" minute(s) and "+(totaltime%60000)/1000+" second(s) to run.");
								return;
						}else{
								stateMiles[trueState]=data.routes[0].legs[0].distance["text"];
						}
						trueState = orderedLatLngs[compileMilesIndex][1];
						compileMilesIndex++;
						setTimeout(compileMiles, 3000);
				}
		}
 }

// GOOGLE MAP - CURRENT POSITION
	$(document).ready(function() {
		// If the browser supports the Geolocation API
		if (typeof navigator.geolocation == "undefined") {
			$("#error").text("Your browser doesn't support the Geolocation API");
			return;
		}

			$("#from-link").click(function(event) {
				event.preventDefault();
				var addressId = this.id.substring(0, this.id.indexOf("-"));

				navigator.geolocation.getCurrentPosition(function(position) {
					var geocoder = new google.maps.Geocoder();
					geocoder.geocode({
						"location": new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
					},
					function(results, status) {
						if (status == google.maps.GeocoderStatus.OK)
							$("#" + addressId).val(results[0].formatted_address);
						else
							$("#error").append("Unable to retrieve your address<br />");
					});
				},
				function(positionError){
					$("#error").append("Error: " + positionError.message + "<br />");
				},
				{
					enableHighAccuracy: true,
					timeout: 10 * 1000 // 10 seconds
				});
			});

			$("#calculate-route").submit(function(event) {
				event.preventDefault();
				calculateRoute($("#from").val(), $("#to").val());
			});
			
			$(".to").click(function() {
			this.value = '';
			});
			
		});

</script>
