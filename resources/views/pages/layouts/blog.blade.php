@extends('pages.layouts.master')

@section('page-header')

{{-- Header --}}		 
<header class="intro-header" style="background-image: url('/uploads/img/diplomas.jpg')">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
				<div class="site-heading">
					<h1>{{ $title }}</h1>
					<hr class="small">
					<h2 class="subheading">{{ $subtitle }}</h2>
				</div>
			</div>
		</div>
	</div>
</header>

@stop

@section('content')

{{-- The Blog --}}
<div id="blogPageContent" class="container">
	<div class="row">
		
		@include('pages.partials.success')
		
		{{-- The Blog List --}}
		
		<div id="boxes2" class="col-sm-8 col-md-7 col-md-offset-1 col-lg-7">
		<div id="boxes">
			{{-- The Posts --}}
			@foreach ($posts as $post)
				<div class="post-preview" style="width:100%; margin-bottom: 50px;">
					<a href="{{ $post->url($tag) }}">
						<h2 class="post-title">{{ $post->title }}</h2>
						@if ($post->subtitle)
							<h3 class="post-subtitle">{{ $post->subtitle }}</h3>
						@endif
					</a>
					<p class="post-meta pull-right">
						Posted on {{ $post->published_at->format('F j, Y') }}
						@if ($post->tags->count())
							in
							{!! join(', ', $post->tagLinks()) !!}
						@endif
					</p>
				</div>
				<br>
			@endforeach
					{{-- The Pager --}}
			<ul class="pager">
				{{-- Reverse direction --}}
				@if ($reverse_direction)
					@if ($posts->currentPage() > 1)
						<li class="previous">
							<a href="{!! $posts->url($posts->currentPage() - 1) !!}">
								<i class="fa fa-long-arrow-left fa-lg"></i>
								Previous {{ $tag->tag }} Posts
							</a>
						</li>
					@endif
					@if ($posts->hasMorePages())
						<li class="next">
							<a href="{!! $posts->nextPageUrl() !!}">
								Next {{ $tag->tag }} Posts
								<i class="fa fa-long-arrow-right"></i>
							</a>
						</li>
					@endif
				@else
					@if ($posts->currentPage() > 1)
						<li class="previous">
							<a href="{!! $posts->url($posts->currentPage() - 1) !!}">
								<i class="fa fa-long-arrow-left fa-lg"></i>
								Newer {{ $tag ? $tag->tag : '' }} Posts
							</a>
						</li>
					@endif
					@if ($posts->hasMorePages())
						<li class="next">
							<a href="{!! $posts->nextPageUrl() !!}">
								Older {{ $tag ? $tag->tag : '' }} Posts
								<i class="fa fa-long-arrow-right"></i>
							</a>
						</li>
					@endif
				@endif
			</ul>
	
		</div>
		</div>


	</div>
</div>

@stop

@section('footer')

	@include('pages.partials.blog-footer')
	
@stop

@section('scripts')
	
	@include('pages.partials.social.scripts')

@stop