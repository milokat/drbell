@extends('pages.layouts.master', ['meta_description' => 'Home Page'])
    
@section('content')
<div id="homePageContent">

{{-- Hero Image --}} 
<header class="image-bg-fixed-height">
	<img class="img-fixed img-center" src="" style="max-height:300px;" alt=""></img>
			{{-- Content Section --}}
			<section class="container-fluid">
				{{-- Social Connect Button --}} 
				<div id="connectMenu" class="connectPageButton">
					<div class="row">
						<div class="connectPanelFixed">
							<div id="connectDropdown" class="dropdown" style="width:50%;">
								@include('pages.partials.social.connect-panel')
							</div>
						</div>
					</div>
				</div>
				<div class="row homebox">
					<div class="col-sm-8 col-sm-offset-1 happy">
						<h1 class="section-heading">Bell Chiropractic</h1>
							<p class="lead section-lead">We help you alleviate discomfort to keep your body feeling solid.</p>
							<p class="section-paragraph" style="padding-right: 20px;">If you’re a young adult just beginning to feel wear and tear, take preventative measures to help your body remain secure and balanced. 
							</p>
					</div>
					<div class="col-sm-3 shareBox">
						<div class="row">
							<h2>Tell your friends!</h2>

								{{-- The Shortlink --}}
								<h5 style="color:rgba( 166, 217, 189, 1.0);">
										Share this page!
										<br />
										<br />
									<a class="shortLink underline" href="{{ $shortLink }}">
										{{ $shortLink }}
									</a>
								</h5>

								{{-- Subscriber Button --}}
								<div class="row-fluid connectModal">
									<a href="#subscriberModal" class="btn btn-info btn-xs" data-toggle="modal" style="font-size:12px;">
									<i class="fa fa-plus"></i> 
										Join our Newsletter!
									</a>
								</div>
									@include('pages.partials.modals.subscribe')
									
						</div>
					</div>
			  </div>
			</section>
</header>

{{-- Fixed Height Image Spacer --}}       
<div class="image-bg-fixed-height2"></div>

{{-- Page Featurettes --}}
<div class="row text-center featuresModule">
	<div class="container-fluid">
		<h3>We assist your body to maintain strength and support no matter your age.</h3>
			<div class="container">

				<div class="col-md-4 col-sm-4 hero-feature">
					<div class="thumbnail">
						<img src="/uploads/img/files-1.jpg" alt="">
							<div class="caption">
									<p>Relieve stressful pains and discomfort.</p>
									<p>
									<a id="fuck" href="/services" class="btn btn-default featureBtn">Our Services</a>
									</p>
							</div>
					</div>
				</div>

				<div class="col-md-4 col-sm-4 hero-feature">
					<div class="thumbnail">
						<img src="/uploads/img/portrait-2.jpg" alt="">
							<div class="caption">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
									<p>
									<a href="/contact" class="btn btn-default featureBtn">Greetings</a>
									</p>
							</div>
					</div>
				</div>

				<div class="col-md-4 col-sm-4 hero-feature">
					<div class="thumbnail">
						<img src="/uploads/img/posters-1.jpg" alt="">
							<div class="caption">
									<p>Explore our blog for the latest news and promotions.</p>
									<p>
									<a href="/blog" class="btn btn-default featureBtn">Our Blog</a>
									</p>
							</div>
					</div>
				</div>
				
			</div>
	</div>
</div>

{{-- Fixed Height Image Spacer --}}		
<div class="image-bg-fixed-height2"></div>

{{-- Mission Statement --}} 
<div class="row" style="background-color:rgba( 26, 38, 51, 1.0);">
	<div class="container" style="padding:50px;">		
		<div class="col-md-6">
			<h1 style="color:rgba( 237, 230, 206, 1.0);">Our Goal?</h1>
				<p style="color:rgba( 237, 230, 206, 1.0); margin-bottom:15px;">
				Relieving pain as naturally as possible, while advising you how to avoid future injury from reoccurring.
<br />			
				</p>
				<a href="/contact" class="underline">
				Lets get started.
				</a>
		</div>
		<div class="col-md-6" style="padding:20px;">
			<img class="img-responsive img-rounded feature-img" src="/uploads/img/building-2.jpg" alt="">
		</div>
	</div>
</div>

{{-- Call to Action Well --}}
<div class="row">
	<div class="col-lg-12">
		<div class="well text-center">
				<span class="underline">Call-ins welcome.</span> Most appointments same day! <span class="phone">{{ config('business-details.phone') }}</span>
		</div>
	</div>
</div>
</div>
@stop

@section('footer')

	@include('pages.partials.page-footer')
	
@stop

@section('scripts')

	@include('pages.partials.map.scripts')
	@include('pages.partials.social.scripts')
	
@stop