<!DOCTYPE html>
<html lang="en" 
      xmlns="http://www.w3.org/1999/xhtml"
    	prefix="og: http://ogp.me/ns# 
             fb: http://ogp.me/ns/fb#"
<head>
@include('pages.partials.config')
<meta name="description" content="{{ $meta_description }}">
<meta name="author" content="{{ config('blog.author') }}">
<link rel="alternate" type="application/rss+xml" href="{{ url('rss') }}"
   	  title="RSS Feed {{ config('blog.title') }}">

<title>{{ $title or config('blog.title') }}</title>

@include('pages.partials.social.meta')

{{-- Styles --}}
<link href="/assets/css/drbell.css" rel="stylesheet">
@yield('styles')

{{-- HTML5 Shim and Respond.js for IE8 support --}}
<!--[if lt IE 9]>
<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body id="pageBody" style="overflow-x: hidden;">
@include('pages.partials.page-nav')
@yield('page-header')
@yield('content')

<footer>
@yield('footer')
</footer>

<script src="/assets/js/drbell.js"></script>
@yield('scripts')

</body>
</html>