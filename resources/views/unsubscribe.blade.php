@extends('pages.layouts.master', ['meta_description' => 'Contact Form'])
   
@section('page-header')
  <header class="intro-header">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
          <div class="site-heading">
            <h1></h1>
            <hr class="small">
            <h2 class="subheading"></h2>
          </div>
        </div>
      </div>
    </div>
  </header>
@stop   
   
@section('content')
    <div class="container">
        @include('pages.partials.errors')
        @include('pages.partials.success')
         
         <form action="/subscribers/unsubscribe" method="post">
          <input type="hidden" name="_token" value="{!! csrf_token() !!}">
           <input type="hidden" name="_method" value="DELETE">
            <div class="row control-group">
            <div class="form-group col-xs-12 col-sm-8 col-md-6">
              <label for="email">Enter the Email Address to Unsubscribe</label>
              <input type="email" class="form-control" id="email" name="email"
                     value="{{ old('email') }}"></input>
            </div>
            </div>
          <br>
          <div class="row">
            <div class="form-group col-xs-12">
              <button type="submit" class="btn btn-default">UnSubscribe</button>
            </div>
          </div>
        </form>
        
       </div>
@stop