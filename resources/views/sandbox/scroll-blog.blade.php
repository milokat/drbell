@extends('pages.layouts.master')


@section('styles')
<link href="/assets/css/blog.css" rel="stylesheet">
@stop

@section('page-header')
 
<header class="intro-header" {{-- style="background-image: url('{{ page_image($page_image) }}')" --}}>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
				<div class="site-heading">
					<h1>{{ $title }}</h1>
					<hr class="small">
					<h2 class="subheading">{{ $subtitle }}</h2>
				</div>
			</div>
		</div>
	</div>
</header>

@stop

@section('content')

<div class="container">
	<div class="row">
		
		@include('pages.partials.success')
		
		{{-- The Blog List --}}
		
		<div id="boxes2" class="col-sm-8 col-md-7 col-md-offset-1 col-lg-7">
		<div id="boxes">
			{{-- The Posts --}}
			@foreach ($posts as $post)
				<div class="post-preview" style="width:100%; margin-bottom: 50px;">
					<a href="{{ $post->url($tag) }}">
						<h2 class="post-title">{{ $post->title }}</h2>
						@if ($post->subtitle)
							<h3 class="post-subtitle">{{ $post->subtitle }}</h3>
						@endif
					</a>
					<p class="post-meta pull-right">
						Posted on {{ $post->published_at->format('F j, Y') }}
						@if ($post->tags->count())
							in
							{!! join(', ', $post->tagLinks()) !!}
						@endif
					</p>
				</div>
				<br>
			@endforeach
		</div>
		</div>
		 <div class="paginate">
        {!! $posts->render() !!}
    </div>

		{{-- Connect Panel --}}
		<aside class="container" id="connectMenu">
			<div class="connectPanel hidden-xs col-sm-1 col-sm-offset-2 col-md-1 col-lg-1 col-lg-offset-2" style="right: 40px;">
				<div data-spy="affix" id="connectDropdown" class="dropdown">
						@include('pages.partials.social.connect-panel')
				</div>
			</div>
		</aside>
		
	</div>
</div>

@stop

@section('footer')

	@include('pages.partials.blog-footer')
	
@stop

@section('scripts')
	
	@include('pages.partials.social.scripts')
	<script src="/assets/infinitescroll/jquery.infinitescroll.min.js"></script>
	<script src="/assets/masonry/masonry.pkgd.min.js"></script>
	<script>
	$( document ).ready(function() {
			
				$('#boxes').masonry({
					itemSelector: '.post-preview',
					transitionDuration: '1.0s'
				});

				$('#boxes2').infinitescroll({
					loading: {
					finished: undefined,
					finishedMsg: "<div><p><em>This is the end, my only friend.</em><div><p>",
					msgText: "<em>Loading the next set of posts...</em>",
					speed: 'normal',
					start: undefined
				},
					navSelector     : ".paginate",
					nextSelector    : ".paginate a:last",
					itemSelector    : ".post-preview",
					debug           : false,
					dataType        : 'html',
				  bufferPx   			: 40,
					
					path: function(index) {
							return "?page=" + index;
					}
			}, function(newElements, data, url){
				var $newElems = $( newElements );
				$('#boxes').masonry( 'appended', $newElems, true);
				});
		});
	</script>
@stop